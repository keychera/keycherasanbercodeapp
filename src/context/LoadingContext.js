import React, { createContext, useState } from 'react'
import AsyncStorage from '@react-native-community/async-storage'
import Loading from '../screens/Loading'
import { View } from 'react-native'

export const LoadingContext = createContext()

const LoadingContextContainer = ({ children }) => {

  const [currentlyLoading, setCurrentlyLoading] = useState([])

  const declareLoading = (key) => {
    setCurrentlyLoading([
      ...currentlyLoading, key
    ])
  }

  const declareFinished = (key) => {
    setCurrentlyLoading(currentlyLoading.filter(
      (v) => v != key
    ))
  }

  const isLoading = (currentlyLoading.length > 0)

  return (
    <LoadingContext.Provider value={{ declareLoading, declareFinished }}>
      {
        isLoading
          ? (
            <View style={{ flex: 1, zIndex: 1 }}>
              <Loading info={currentlyLoading} />
            </View>
          )
          : (<></>)
      }
      <View style={{ flex: 1, display: isLoading ? "none" : "flex" }}>
        {children}
      </View>
    </LoadingContext.Provider>
  )
}

export default LoadingContextContainer