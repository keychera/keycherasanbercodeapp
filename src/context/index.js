import React from 'react';
import AuthContextContainer from './AuthContext';
import IntroContextContainer from './IntroContext';
import LoadingContextContainer from './LoadingContext';

const MainContextApp = ({ children }) => {
  return (
    <LoadingContextContainer>
      <AuthContextContainer>
        <IntroContextContainer>
          {children}
        </IntroContextContainer>
      </AuthContextContainer>
    </LoadingContextContainer>
  )
}

export default MainContextApp
