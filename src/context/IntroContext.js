import React, { createContext, useState, useContext } from 'react'
import AsyncStorage from '@react-native-community/async-storage'
import Loading from '../screens/Loading'
import { View } from 'react-native'
import { LoadingContext } from './LoadingContext'

export const IntroContext = createContext()

const IntroContextContainer = ({ children }) => {

  const [firstToken, setFirstToken] = useState(null)

  const loadingState = useContext(LoadingContext)
  const { declareLoading, declareFinished } = loadingState


  const hasBeenHere = async () => {
    declareLoading('intro1')
    try {
      await AsyncStorage.setItem('first', 't')
      setFirstToken(true)
    } catch (e) {
      console.log(e)
    }
    declareFinished('intro1')
  }

  const resetIntro = async () => {
    declareLoading('intro2')
    try {
      setFirstToken(null)
      await AsyncStorage.removeItem('first')
    } catch (e) {
      console.log(e)
    }
    declareFinished('intro2')
  }

  const getIsFirstTime = async () => {
    declareLoading('intro3')
    try {
      const value = await AsyncStorage.getItem('first')
      setFirstToken(value !== null)
    } catch (e) {
      console.log(e)
    }
    declareFinished('intro3')
  }


  React.useEffect(() => {
    getIsFirstTime()
  }, [])

  return (
    <IntroContext.Provider value={{ firstToken, hasBeenHere, resetIntro }}>
      {children}
    </IntroContext.Provider>
  )
}

export default IntroContextContainer