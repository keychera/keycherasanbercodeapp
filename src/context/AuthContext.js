import React, { createContext, useState, useContext } from 'react'
import AsyncStorage from '@react-native-community/async-storage'
import Loading from '../screens/Loading'
import auth from '@react-native-firebase/auth'
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin'
import TouchID from 'react-native-touch-id'
import { Alert, View } from 'react-native'
import { LoadingContext } from './LoadingContext'

const fingerprintConfig = {
  title: 'Authentication required',
  imageColor: '#191970',
  imageErrorColor: 'red',
  sensorDescription: 'Touch Sensor',
  sensorErrorDescription: 'Failed',
  cancelText: 'Cancel'
}

export const AuthContext = createContext()

const AuthContextContainer = ({ children }) => {

  const [userInput, setUserInput] = useState('')
  const [passInput, setPassInput] = useState('')
  const [currentToken, setCurrentToken] = useState(null)

  const loadingState = useContext(LoadingContext)
  const { declareLoading, declareFinished } = loadingState

  const _storeUserToken = async (token) => {
    try {
      await AsyncStorage.setItem('token', token);
    } catch (error) {
      alert('Error when saving the token')
    }
  };

  const _deleteUserToken = async () => {
    try {
      await AsyncStorage.removeItem('token');
    } catch (error) {
      alert('Error when deleting the token')
    }
  };

  const _retrieveUserToken = async () => {
    try {
      return await AsyncStorage.getItem('token');
    } catch (error) {
      alert('Error when retrieving the token')
    }
  };

  const signInWithEmail = () => {
    declareLoading('auth1')
    auth().signInWithEmailAndPassword(userInput, passInput)
      .then(async userCred => {
        await login(userCred.user.uid)
        declareFinished('auth1')
      })
      .catch(error => {
        var errorCode = error.code
        var errorMessage = error.message
        if (errorCode === 'auth/wrong-password') {
          alert('Wrong password.')
        } else {
          alert(errorMessage)
        }
        console.log(error)
        declareFinished('auth1')
      })
  }

  // const signInWithEmail = async () => {
  //   declareLoading('auth2')
  //   const response = await fetch('https://mainbersama.demosanbercode.com/api/login', {
  //     method: 'POST',
  //     headers: {
  //       'Content-Type': 'application/json'
  //     },
  //     body: JSON.stringify({
  //       email: userInput,
  //       password: passInput
  //     })
  //   })
  //   const content = await response.json()
  //   const { success } = content
  //   if (success) {
  //     const { token } = content
  //     await login(token)
  //   } else {
  //     alert('Login Failed')
  //   }
  //   declareFinished('auth2')
  //   return success
  // }

  const signinWithGoogle = async () => {
    try {
      const { idToken } = await GoogleSignin.signIn()
      const credential = auth.GoogleAuthProvider.credential(idToken)
      declareLoading('auth3')
      await auth().signInWithCredential(credential)
      await login(idToken)
      declareFinished('auth3')
    } catch (error) {
      console.log(error);
    }
  }

  const signInWithFingerprint = () => {
    TouchID.authenticate('', fingerprintConfig)
      .then(async success => {
        declareLoading('auth4')
        await login('fingerprint')

        declareFinished('auth4')
      })
      .catch(error => Alert.alert('Error signing with fingerprint', error.details))
  }

  const login = async (token) => {
    await _storeUserToken(token)
    setCurrentToken(token)
  }

  const logout = async () => {
    declareLoading('auth5')
    setCurrentToken(null)
    try {
      await GoogleSignin.revokeAccess()
      await GoogleSignin.signOut()
    } catch (error) {
      console.log(error)
    }
    await _deleteUserToken()
    declareFinished('auth5')
  }

  const _setCurrentToken = async () => {
    declareLoading('auth6')
    const token = await _retrieveUserToken()
    setCurrentToken(token)
    declareFinished('auth6')
  }

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId: '752348763163-1tsjlci3oi5sua9j62eu0u23ed5qrpkm.apps.googleusercontent.com'
    })
  }

  React.useEffect(() => {
    configureGoogleSignIn()
    _setCurrentToken()
  }, [])

  return (
    <AuthContext.Provider value={{
      userInput, setUserInput,
      passInput, setPassInput,
      currentToken,
      signInWithEmail,
      signInWithFingerprint,
      signinWithGoogle,
      login, logout,
    }}>
      {children}
    </AuthContext.Provider>
  )
}

export default AuthContextContainer