import React, { createContext, useContext } from 'react';
import AppNavigation from './navigation'
import SplashScreen from './screens/Splashscreen'
import MainContextApp from './context';

const MainApp = () => {
  const [isLoading, setIsLoading] = React.useState(true)

  React.useEffect(() => {
    if (isLoading) {
      setTimeout(() => {
        setIsLoading(!isLoading)
      }, 3000)
    }
  }, [])

  return (
    <>
      <MainContextApp>
        {isLoading
          ? (<SplashScreen />)
          : (<AppNavigation />)
        }
      </MainContextApp>
    </>
  )
}

export default MainApp