import React, { useEffect, useState } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import auth from '@react-native-firebase/auth'
import database from '@react-native-firebase/database'
import { GiftedChat } from 'react-native-gifted-chat'

const Chat = () => {
  const [messages, setMessages] = useState([])
  const [user, setUser] = useState({})

  const onSend = (messages = []) => {
    for (const idx in messages) {
      const message = messages[idx];
      database().ref('messages').push({
        _id: message._id,
        createAt: database.ServerValue.TIMESTAMP,
        text: message.text,
        user: message.user
      })
    }
  }

  const getData = () => {
    database().ref('messages').limitToLast(20).on('child_added', snapshot => {
      const value = snapshot.val()
      setMessages(prevMessage => GiftedChat.append(prevMessage, value))
    })
  }

  useEffect(() => {
    const user = auth().currentUser
    setUser(user)
    getData()
    return () => {
      const db = database().ref('messages')
      if (db) { db.off() }
    }
  }, [])

  return (
    <GiftedChat
      messages={messages}
      onSend={messages => onSend(messages)}
      user={{
        _id: user.uid,
        name: user.email,
        avatar: user.photoURL
      }}
      renderUsernameOnMessage={true}
    />
  )
}

const styles = StyleSheet.create({

})

export default Chat
