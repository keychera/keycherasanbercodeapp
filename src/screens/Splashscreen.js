import React from 'react'
import { StyleSheet, View, Text, Image } from 'react-native'

const Splashscreen = () => (
  <View style={styles.container}>
    <Image style={styles.logo}
      source={require('../assets/images/logo.jpg')}
      resizeMode="contain"
    />
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  logo: {
    height: 180,
  }
})

export default Splashscreen