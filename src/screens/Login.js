import React, { useContext } from 'react'
import { StyleSheet, View, Text, Image, TextInput, TouchableOpacity, Alert } from 'react-native'
import { AuthContext } from '../context/AuthContext'
import auth from '@react-native-firebase/auth'
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin'
import TouchID from 'react-native-touch-id'
import { IntroContext } from '../context/IntroContext'

const Login = ({ navigation }) => {
  const authState = useContext(AuthContext)
  const {
    userInput, passInput, setUserInput, setPassInput,
    signInWithEmail, signinWithGoogle, signInWithFingerprint
  } = authState

  const introState = useContext(IntroContext)
  const { resetIntro } = introState

  return (
    <View style={styles.container}>
      <Image style={styles.logo}
        source={require('../assets/images/logo.jpg')}
        resizeMode="contain"
      />
      <View style={styles.form}>
        <Text style={styles.formLabel}>Username</Text>
        <TextInput
          style={styles.formInput}
          value={userInput}
          onChangeText={setUserInput}
          placeholder="Username or Email"
        />
        <Text style={styles.formLabel}>Password</Text>
        <TextInput
          style={styles.formInput}
          value={passInput}
          onChangeText={setPassInput}
          placeholder="Password" secureTextEntry={true}
        />

        <TouchableOpacity
          style={[styles.confirmButton, { backgroundColor: "#3EC6FF", }]}
          onPress={() => {
            if (userInput.length != 0 && passInput.length != 0) {
              signInWithEmail()
            } else {
              alert('Please fill the username and password first')
            }
          }}
        >
          <Text style={styles.confirmButtonText}>LOGIN</Text>
        </TouchableOpacity>

        <Text style={{ alignSelf: "center" }}>OR</Text>

        <GoogleSigninButton
          onPress={signinWithGoogle}
          style={[styles.confirmButton, { alignSelf: "center", }]}
          size={GoogleSigninButton.Size.Wide}
          color={GoogleSigninButton.Color.Dark}
        />
        <TouchableOpacity
          style={[styles.confirmButton, { backgroundColor: "#191970", }]}
          onPress={signInWithFingerprint}
        >
          <Text style={styles.confirmButtonText}>SIGN IN WITH FINGERPRINT</Text>
        </TouchableOpacity>
        <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", paddingTop: 15 }}>
          <TouchableOpacity onPress={resetIntro}>
            <Text>Reset Intro</Text>
          </TouchableOpacity>
          <Text>•</Text>
          <TouchableOpacity onPress={() => navigation.navigate('Register')}>
            <Text>Create an account</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View >
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    flexDirection: "column",
    alignItems: "stretch",
  },
  logo: {
    height: 180,
    marginVertical: 20,
    alignSelf: "center"
  },
  form: {
    flex: 1,
    paddingHorizontal: 30
  },
  formLabel: {
    marginTop: 5
  },
  formInput: {
    borderBottomColor: "lightgray",
    borderBottomWidth: 1,
    paddingHorizontal: 10,
  },
  confirmButton: {
    paddingVertical: 10,
    elevation: 4,
    alignItems: "center",
    marginVertical: 10
  },
  confirmButtonText: {
    color: "white"
  }
})

export default Login
