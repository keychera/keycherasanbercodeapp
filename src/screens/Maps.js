import React from 'react'
import { StyleSheet, View, Text, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import MapboxGL from '@react-native-mapbox-gl/maps'

MapboxGL.setAccessToken(
  "pk.eyJ1Ijoia2V5Y2hlcmEiLCJhIjoiY2tldGpxczJhMTR1ajJ0b21yYnMzcWRpdyJ9.4f-_XjdhrgKW1mIvJ9-HbA"
)

const coordinates = [
  [107.598827, -6.896191],
  [107.596198, -6.899688],
  [107.618767, -6.902226],
  [107.621095, -6.898690],
  [107.615698, -6.896741],
  [107.613544, -6.897713],
  [107.613697, -6.893795],
  [107.610714, -6.891356],
  [107.605468, -6.893124],
  [107.609180, -6.898013]
]

const Maps = () => {

  const getLocation = async () => {
    try {
      const permission = await MapboxGL.requestAndroidLocationPermissions()
    } catch (e) {
      console.log(e)
    }
  }

  const renderPointAnnotation = (coordinate, i) => {
    const key = `${coordinate[0]}${coordinate[1]}${i}`
    return (
      <MapboxGL.PointAnnotation
        id={key}
        key={key}
        coordinate={coordinate}
      >
        <MapboxGL.Callout
          title={`Longitude${coordinate[0]}\nLatitude${coordinate[1]}`}
        />
      </MapboxGL.PointAnnotation>
    )
  }

  React.useEffect(() => {
    getLocation()
  }, [])

  return (
    <View style={styles.container}>
      <MapboxGL.MapView
        style={styles.mapView}
      >
        <MapboxGL.UserLocation visible={true} />
        <MapboxGL.Camera followUserLocation={true} />
        {coordinates.map(renderPointAnnotation)}
      </MapboxGL.MapView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: "white",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  mapView: {
    flex: 1,
    width: '100%',
  }
})

export default Maps
