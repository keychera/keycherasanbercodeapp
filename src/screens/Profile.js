import React, { useState, useContext } from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import { GoogleSignin } from '@react-native-community/google-signin'
import { AuthContext } from '../context/AuthContext'

const DetailRow = ({ label, children }) => (
  <View style={styles.detailRow}>
    <Text>{label}</Text>
    <Text>{children}</Text>
  </View>
)

const Profile = ({ navigation }) => {

  const [userInfo, setUserInfo] = useState({
    photo: require('../assets/images/hwgift.jpg'),
    name: 'Kevin Erdiza Yogatama',
    birthdate: '14 Agustus 1997',
    sex: 'laki-laki',
    hobby: 'menggambar',
    phone_no: '-private info-',
    email: '-private info-',
  })

  const state = useContext(AuthContext)
  const { logout } = state

  const getCurrentUser = async () => {
    try {
      const googleUserInfo = await GoogleSignin.signInSilently()
      setUserInfo({
        ...userInfo,
        email: googleUserInfo.user.email,
        name: googleUserInfo.user.name,
        photo: { uri: googleUserInfo.user.photo },
      })
    } catch (error) {
      console.log(error)
    }
  }

  React.useEffect(() => {
    getCurrentUser()
  }, [])

  return (
    <View style={styles.container}>
      <View style={styles.profileContainer}>
        <View style={styles.imageContainer}>
          <Image style={styles.profileImage} source={userInfo.photo} />
        </View>
        <Text style={styles.profileName}>{userInfo.name}</Text>
        <View style={styles.detailsContainer}>
          <DetailRow label="Tanggal Lahir">{userInfo.birthdate}</DetailRow>
          <DetailRow label="Jenis Kelamin">{userInfo.sex}</DetailRow>
          <DetailRow label="Hobi">{userInfo.hobby}</DetailRow>
          <DetailRow label="No. Telp">{userInfo.phone_no}</DetailRow>
          <DetailRow label="Email">{userInfo.email}</DetailRow>
          <TouchableOpacity
            style={styles.logoutButton}
            onPress={logout}
          >
            <Text style={styles.logoutText}>LOGOUT</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.extraBackground} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#3EC6FF"
  },
  profileContainer: {
    flex: 0.55,
    alignItems: "center",
    paddingVertical: 30,
    paddingHorizontal: 10
  },
  imageContainer: {
    width: 100,
    height: 100,
    elevation: 10,
    borderRadius: 50,
    marginVertical: 15
  },
  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  profileName: {
    marginVertical: 15,
    fontSize: 20,
    fontWeight: "bold",
    color: "white"
  },
  detailsContainer: {
    marginVertical: 15,
    marginHorizontal: 10,
    backgroundColor: "white",
    elevation: 10,
    padding: 15,
    borderRadius: 20,
    alignSelf: "stretch"
  },
  detailRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 5
  },
  logoutButton: {
    alignSelf: "stretch",
    alignItems: "center",
    padding: 10,
    backgroundColor: "#3EC6FF",
    marginTop: 10
  },
  logoutText: {
    color: "white"
  },
  extraBackground: {
    flex: 1,
    backgroundColor: "white",
    zIndex: -1
  }
})

export default Profile