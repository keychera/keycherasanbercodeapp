import React, { useState, useContext } from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity, TextInput, Modal } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import { RNCamera } from 'react-native-camera';
import storage from '@react-native-firebase/storage'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { AuthContext } from '../context/AuthContext';

const RegisterRow = ({ label, placeholder, value, onChangeText, secureTextEntry }) => (
  <View style={styles.registerRow}>
    <Text>{label}</Text>
    <TextInput
      value={value}
      onChangeText={onChangeText}
      placeholder={placeholder ? placeholder : label}
      secureTextEntry={secureTextEntry}
    />
  </View>
)

const Register = ({ navigation }) => {
  const [isCameraVisible, setIsCameraVisible] = useState(false)
  const [cameraType, setCameraType] = useState('back')
  const [registerPhoto, setRegisterPhoto] = useState(require('../assets/images/hwgift.jpg'))
  const [isUploading, setIsUploading] = useState(false)

  const state = useContext(AuthContext)
  const { login } = state

  let camera = null
  const renderCamera = () => {
    return (
      <Modal visible={isCameraVisible} onRequestClose={() => setIsCameraVisible(false)}>
        <View style={{ flex: 1 }}>
          <RNCamera
            style={styles.rnCamera}
            type={cameraType}
            ref={ref => { camera = ref }}
            captureAudio={false}
          >
            <TouchableOpacity
              style={styles.cameraFlipButton}
              onPress={toggleCameraType}
            >
              {
                cameraType == 'back'
                  ? (<Icon style={styles.cameraFlipIcon} name='camera-front' />)
                  : (<Icon style={styles.cameraFlipIcon} name='camera-rear' />)
              }
            </TouchableOpacity>
            <View style={styles.assistLine} />
            <View style={styles.assistLine2} />
            <TouchableOpacity
              style={styles.cameraTakeButton}
              onPress={takePicture}
            >
              <Icon style={styles.cameraTakeIcon} name='camera-outline' />
            </TouchableOpacity>
          </RNCamera>
        </View>
      </Modal>
    )
  }

  const toggleCameraType = () => setCameraType(cameraType == 'back' ? 'front' : 'back')

  const takePicture = async () => {
    const options = { quality: 0.5, base64: true }
    if (camera) {
      const data = await camera.takePictureAsync(options)
      if (data) {
        setRegisterPhoto({ uri: data.uri })
      }
      setIsCameraVisible(false)
    }
  }

  const handleOnRegister = async () => {
    if (registerPhoto.uri) {
      await uploadImage(registerPhoto.uri)
      login('register')
    } else {
      alert('Please take a picture first.')
    }
  }

  const uploadImage = async (uri) => {
    const sessionId = new Date().getTime()
    setIsUploading(true)
    const res = await storage()
      .ref(`images/${sessionId}`)
      .putFile(uri)
      .then((response) => alert('Upload success!'))
      .catch((error) => alert('Upload failed!'))
    setIsUploading(false)
    return res
  }

  return (
    <View style={styles.container}>
      <View style={styles.registerContainer}>
        <View style={styles.imageContainer}>
          <Image style={styles.registerImage} source={registerPhoto} />
        </View>
        <TouchableOpacity
          onPress={() => setIsCameraVisible(true)}
        >
          <Text style={styles.changePictureText}>change picture</Text>
        </TouchableOpacity>
        <View style={styles.detailsContainer}>
          <RegisterRow label="Nama" />
          <RegisterRow label="Email" />
          <RegisterRow label="Password" secureTextEntry/>
          <TouchableOpacity
            style={[styles.registerButton, isUploading ? { backgroundColor: "gray" } : {}]}
            onPress={isUploading ? () => { } : handleOnRegister}
            disabled={isUploading}
          >
            <Text style={styles.registerText}>{isUploading ? 'LOADING...' : 'REGISTER'}</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.extraBackground} />
      {renderCamera()}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#3EC6FF"
  },
  registerContainer: {
    flex: 0.55,
    alignItems: "center",
    paddingVertical: 30,
    paddingHorizontal: 10
  },
  imageContainer: {
    width: 100,
    height: 100,
    elevation: 10,
    borderRadius: 50,
    marginVertical: 15
  },
  registerImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  changePictureText: {
    marginVertical: 15,
    fontSize: 20,
    fontWeight: "bold",
    color: "white"
  },
  detailsContainer: {
    marginVertical: 15,
    marginHorizontal: 10,
    backgroundColor: "white",
    elevation: 10,
    padding: 15,
    borderRadius: 20,
    alignSelf: "stretch"
  },
  registerRow: {
    marginVertical: 5
  },
  registerButton: {
    alignSelf: "stretch",
    alignItems: "center",
    padding: 10,
    backgroundColor: "#3EC6FF",
    marginTop: 10
  },
  registerText: {
    color: "white"
  },
  extraBackground: {
    flex: 1,
    backgroundColor: "white",
    zIndex: -1
  },
  rnCamera: {
    flex: 1,
    justifyContent: 'space-between',
  },
  cameraFlipButton: {
    width: 40,
    height: 40,
    borderRadius: 30,
    marginLeft: 20,
    marginTop: 30,
    padding: 10,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center'
  },
  cameraFlipIcon: {
    fontSize: 20,
  },
  assistLine: {
    flex: 0.4,
    marginHorizontal: 100,
    borderRadius: 80,
    borderWidth: 1,
    borderColor: 'white'
  },
  assistLine2: {
    flex: 0.25,
    marginHorizontal: 100,
    borderWidth: 1,
    borderColor: 'white'
  },
  cameraTakeButton: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginBottom: 40,
    backgroundColor: 'white',
    alignSelf: "center",
    justifyContent: 'center',
    alignItems: 'center'
  },
  cameraTakeIcon: {
    fontSize: 30,
  }
})

export default Register