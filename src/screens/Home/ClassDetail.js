import React, { useState } from 'react'
import { StyleSheet, View, Text, processColor } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { BarChart } from 'react-native-charts-wrapper'

const data = [
  { y: [100, 40], marker: ["React Native Dasar", "React Native Lanjutan"] },
  { y: [80, 60], marker: ["React Native Dasar", "React Native Lanjutan"] },
  { y: [40, 90], marker: ["React Native Dasar", "React Native Lanjutan"] },
  { y: [78, 45], marker: ["React Native Dasar", "React Native Lanjutan"] },
  { y: [67, 87], marker: ["React Native Dasar", "React Native Lanjutan"] },
  { y: [98, 32], marker: ["React Native Dasar", "React Native Lanjutan"] },
  { y: [150, 90], marker: ["React Native Dasar", "React Native Lanjutan"] },
]

const ClassDetail = () => {

  const [legend, setLegend] = useState({
    enabled: true,
    textSize: 14,
    form: 'SQUARE',
    formSize: 14,
    xEntrySpace: 10,
    yEntrySpace: 5,
    formToTextSpace: 5,
    wordWrapEnabled: true,
    maxSizePercent: 0.5
  })

  const modifiedMarkerData = data.map((data, i) => {
    let finalMarker = []
    for (let i = 0; i < data.y.length; i++) {
      const y = data.y[i]
      const marker = data.marker[i]
      const combinedYMarker = `${marker}\n${y}`
      finalMarker.push(combinedYMarker)
    }
    return { y: data.y, marker: finalMarker }
  })

  const [chart, setChart] = useState({
    data: {
      dataSets: [{
        values: modifiedMarkerData,
        label: '',
        config: {
          colors: [processColor('#3EC6FF'), processColor('#088dc4')],
          stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
          drawFilled: false,
          drawValues: false,
        }
      }]
    }
  })

  const [xAxis, setXAxis] = useState({
    valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct', 'Nov', 'Des'],
    position: 'BOTTOM',
    drawAxisLine: true,
    drawGridLines: false,
    axisMinimum: -0.5,
    granularityEnabled: true,
    granularity: 1,
    axisMaximum: new Date().getMonth() + 0.5,
    spaceBetweenLabels: 0,
    labelRotationAngle: -45.0,
    limitLines: [{ limit: 115, lineColor: processColor('red'), lineWidth: 1 }]
  })

  const [yAxis, setYAxis] = useState({
    left: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      drawGridLines: false
    },
    right: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      enabled: false
    }
  })

  return (
    <View style={styles.container}>
      <BarChart
        style={styles.flex}
        data={chart.data}
        xAxis={xAxis}
        yAxis={yAxis}
        legend={legend}
        pinchZoom={false}
        doubleTapToZoomEnabled={false}
        chartDescription={{ text: '' }}
        marker={{
          enabled: true,
          markerColor: processColor('grey'),
          textColor: processColor('white'),
          textSize: 14
        }}

      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  flex: {
    flex: 1,
    margin: 10
  }
})

export default ClassDetail
