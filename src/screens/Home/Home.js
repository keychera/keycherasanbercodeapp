import React from 'react'
import { StyleSheet, View, Text, ScrollView, FlatList, Image, Dimensions } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { BarChart } from 'react-native-charts-wrapper'
import { TouchableOpacity } from 'react-native-gesture-handler'

const Home = ({ navigation }) => {
  const data = {
    classList: [
      { className: 'React Native', icon: 'react' },
      { className: 'Data Science', icon: 'language-python' },
      { className: 'ReactJS', icon: 'react' },
      { className: 'Laravel', icon: 'laravel' },
      { className: 'Wordpress', icon: 'wordpress' },
      { className: 'Desain Grafis', image: require('../../assets/images/website-design.png') },
      { className: 'Web Server', icon: 'server' },
      { className: 'UI/UX Design', image: require('../../assets/images/ux.png') },
    ],
    summary: [
      { className: 'React Native', today: 20, total: 100 },
      { className: 'Data Science', today: 30, total: 100 },
      { className: 'ReactJS', today: 66, total: 100 },
      { className: 'Laravel', today: 60, total: 100 },
      { className: 'Laravel', today: 60, total: 100 },
      { className: 'Blender', today: 42, total: 100 },
      { className: 'Blender', today: 42, total: 100 },
      { className: 'Godot', today: 64, total: 100 },
      { className: 'Godot', today: 11, total: 32 },
    ]
  }

  const ClassCard = ({ item }) => {
    const { className, image, icon } = item
    return (
      <TouchableOpacity
        style={styles.classCard}
        onPress={() => {
          navigation.navigate("ClassDetail", {
            title: className,
          })
        }}
      >
        {image
          ? (<Image style={styles.classImage} source={image} resizeMode="center" />)
          : (
            <Icon style={styles.classIcon} name={icon} />)
        }
        <Text style={styles.classTitle}>{className}</Text>
      </TouchableOpacity>
    )
  }

  const SummaryCard = ({ item }) => {
    const { className, today, total } = item
    return (
      <View style={styles.summaryCard} >
        <Text style={styles.summaryTitle}>{className}</Text>
        <View style={styles.summaryDetail}>
          <Text style={styles.summaryText}>Today</Text>
          <Text style={styles.summaryText}>{today} orang</Text>
        </View>
        <View style={styles.summaryDetail}>
          <Text style={styles.summaryText}>Total</Text>
          <Text style={styles.summaryText}>{total} orang</Text>
        </View>
      </View >
    )
  }

  return (
    <View style={styles.container}>
      <View style={styles.homeContainer}>
        <View style={[styles.sectionContainer, styles.flatListContainer]}>
          <Text style={styles.sectionTitle}>Kelas</Text>
          <FlatList
            contentContainerStyle={styles.flatList}
            data={data.classList.slice(0, 4)}
            renderItem={ClassCard}
            keyExtractor={(item, i) => `${item.name}${i}`}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
          />
        </View>
        <View style={[styles.sectionContainer, styles.flatListContainer]}>
          <Text style={styles.sectionTitle}>Kelas</Text>
          <FlatList
            contentContainerStyle={styles.flatList}
            data={data.classList.slice(4, 8)}
            renderItem={ClassCard}
            keyExtractor={(item, i) => `${item.name}${i}`}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
          />
        </View>
        <View style={[styles.listSectionContainer, styles.flatListContainer]}>
          <Text style={styles.sectionTitle}>Summary</Text>
          <FlatList
            contentContainerStyle={styles.flatList}
            data={data.summary}
            renderItem={SummaryCard}
            keyExtractor={(item, i) => `${item.class}${i}`}
            showsVerticalScrollIndicator={false}
          />
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingTop: 20,
    paddingBottom: 100
  },
  homeContainer: {
    width: '90%',
  },
  sectionContainer: {
    marginBottom: 10,
    height: '20%'
  },
  sectionTitle: {
    fontSize: 16,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: '#088dc4',
    color: 'white',
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
  listSectionContainer: {
    height: '63%',
    marginBottom: 10,
  },
  flatListContainer: {
    overflow: "hidden",
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  flatList: {
    backgroundColor: '#3EC6FF',
  },
  classCard: {
    backgroundColor: '#3EC6FF',
    alignItems: "center",
    justifyContent: "center",
    padding: 10
  },
  classImage: {
    height: '65%',
  },
  classIcon: {
    fontSize: 32,
    color: 'white',
    alignSelf: "center"
  },
  classTitle: {
    color: 'white',
    flexWrap: 'wrap'
  },
  summaryCard: {
    flex: 1,
  },
  summaryTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
    backgroundColor: '#3EC6FF',
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
  summaryDetail: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 5,
    paddingHorizontal: 35,
    backgroundColor: '#088dc4',
  },
  summaryText: {
    color: 'white'
  },
})

export default Home
