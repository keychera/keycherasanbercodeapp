import React, { useContext } from 'react'
import { StyleSheet, View, Text, Image } from 'react-native'
import AppIntroSlider from 'react-native-app-intro-slider'
import Icon from 'react-native-vector-icons/Ionicons';
import { IntroContext } from '../context/IntroContext';

const slides = [
  {
    key: 1,
    title: 'Belajar Intensif',
    text: '4 pekan online, 5 hari sepekan, estimasi materi dan tugas 3-4 jam per hari',
    image: require('../assets/images/working-time.png')
  },
  {
    key: 2,
    title: 'Teknologi Populer',
    text: 'Menggunakan bahasa pemrograman populer',
    image: require('../assets/images/research.png'),
  },
  {
    key: 3,
    title: 'From Zero to Hero',
    text: 'Tidak ada syarat minimum skill, cocok untuk pemula',
    image: require('../assets/images/venture.png'),
  },
  {
    key: 4,
    title: 'Training Gratis',
    text: 'Kami membantu Anda mendapatkan pekerjaan / proyek',
    image: require('../assets/images/money-bag.png'),
  }
]

const Intro = ({ navigation }) => {
  const renderSlide = ({ item }) => {
    const { title, text, image } = item
    return (
      <View style={styles.slide}>
        <Text style={styles.slideTitle}>{title}</Text>
        <Image style={styles.slideImage} source={image} resizeMode="contain" />
        <Text style={styles.slideText}>{text}</Text>
      </View>
    )
  }

  const renderDoneButton = () => (
    <View style={styles.sliderButton}>
      <Icon style={[styles.sliderButtonIcon, { fontWeight: "200" }]} name="md-checkmark-sharp" />
    </View>
  )

  const renderNextButton = () => (
    <View style={styles.sliderButton}>
      <Icon style={styles.sliderButtonIcon} name="arrow-forward" />
    </View>
  )

  const state = useContext(IntroContext)
  const { hasBeenHere } = state

  const onDone = async () => {
    await hasBeenHere()
    navigation.navigate('Home')
  }

  return (
    <View style={styles.container}>
      <AppIntroSlider
        data={slides}
        onDone={onDone}
        renderItem={renderSlide}
        renderDoneButton={renderDoneButton}
        renderNextButton={renderNextButton}
        keyExtractor={(item, index) => index.toString()}
        activeDotStyle={{ backgroundColor: '#191970' }}
      />
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  slide: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 24,
  },
  slideTitle: {
    color: "black",
    fontSize: 24,
    fontWeight: "bold",
    marginVertical: 10
  },
  slideImage: {
    height: 150,
    marginVertical: 10
  },
  slideText: {
    color: "#a4a4a6",
    fontSize: 16,
    marginVertical: 10,
    textAlign: "center"
  },
  sliderButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#191970",
    width: 44,
    height: 44,
    borderRadius: 100,
  },
  sliderButtonIcon: {
    fontSize: 24,
    color: "white"
  }
})

export default Intro