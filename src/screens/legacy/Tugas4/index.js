import React, { useState, createContext } from 'react'
import TodoList from './TodoList'

export const RootContext = createContext()

const Context = () => {
  const [currentInput, setCurrentInput] = useState('')
  const [todos, setTodos] = useState([
    {'id': '0','date': '27 Feb 2000', 'message':'Read this updated todo system!'},
    {'id': '1','date': '27 Feb 2000', 'message':'Delete this todo!'},
  ])

  const handleInputChangeText = ((text) => setCurrentInput(text))
  const handleSubmitOnPress = () => {
    setTodos([...todos,
      {'id': String(todos.length),'date': new Date(), 'message':currentInput}
    ])
    setCurrentInput('')
  }
  const handleTodoCardOnDelete = (item) => setTodos(
      todos.filter((todo, i) => todo.id !== item.id
  ))

  return (
    <RootContext.Provider value={{
      currentInput, todos,
      handleInputChangeText,
      handleSubmitOnPress,
      handleTodoCardOnDelete
    }}>
      <TodoList/>
    </RootContext.Provider>
  )
}

export default Context