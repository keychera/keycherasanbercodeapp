import React, { useContext } from 'react'
import { StyleSheet, View, Text, FlatList, TextInput, TouchableOpacity, Keyboard } from 'react-native'
import { RootContext } from './index'

const TodoCard = ({date, message, onDelete}) => (
  <View style={styles.todoCard}>
    <View style={styles.todoCardDetail}>
      <Text>{formatDate(date, (d,m,y) => `${d}/${m}/${y}`)}</Text>
      <Text>{message}</Text>
    </View>
    <TouchableOpacity
      style={styles.todoCardDelButton}
      onPress={onDelete}
    >
      <Text style={styles.todoCardDelText}>🗑</Text>
    </TouchableOpacity>
  </View>
)

const TodoList = () => {
  const state = useContext(RootContext)
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Masukan Todolist!</Text>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.textInput}
          value={state.currentInput}
          onChangeText={state.handleInputChangeText}
          placeholder="Input here"
        />
        <TouchableOpacity
          style={styles.submitButton}
          onPress={() => {
            state.handleSubmitOnPress()
            Keyboard.dismiss()
          }}
        >
          <Text style={styles.submitButtonText}>+</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.todoListContainer}>
        <FlatList
          data={state.todos}
          renderItem={({item}) => 
            TodoCard({...item, 'onDelete': () => state.handleTodoCardOnDelete(item)
          })}
          keyExtractor={item => item.id}
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15
  },
  title: {
    fontSize: 20,
    marginLeft: 5,
    marginVertical: 20
  },
  inputContainer: {
    flex: 0.1,
    flexDirection: "row",
    alignItems: "center"
  },
  textInput: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 5,
    marginRight: 10,
    marginLeft: 5
  },
  submitButton: {
    flex: 0.125,
    alignItems: "center",
    marginRight: 10
  },
  submitButtonText: {
    fontSize: 40
  },
  todoListContainer: {
    flex: 1,
    marginVertical: 15,
    marginHorizontal: 5
  },
  todoCard: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 10,
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderWidth: 2,
    borderRadius: 5,
    borderColor: "lightgray"
  },
  todoCardDetail: {
    flex: 1,
    justifyContent: "center"
  },
  todoCardDelButton: {
    flexDirection: "row",
    flex: 0.2,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#DDDDDD",
    borderRadius: 10,
    margin: 5
  },
  todoCardDelText: {
    fontSize: 30,
  }
})

function formatDate(inputDate, template) {
  const date  = new Date(inputDate)
  const [ year, month, day ] = date.toJSON().substring(0,10).split("-")
  return template(day, month, year)
}

export default TodoList