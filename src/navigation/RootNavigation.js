import React, { useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Intro from '../screens/Intro';
import HomeNavigation from './HomeNavigation';
import { IntroContext } from '../context/IntroContext';

const RootStack = createStackNavigator();
const RootNavigation = () => {

  const state = useContext(IntroContext)
  const { firstToken } = state
  return (
    <RootStack.Navigator>
      {firstToken
        ? (<RootStack.Screen name="Home" component={HomeNavigation} options={{ headerShown: false }} />)
        : (<RootStack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />)
      }
    </RootStack.Navigator>
  )
}

export default RootNavigation