import React, { useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Login from '../screens/Login';
import Profile from '../screens/Profile';
import Register from '../screens/Register';
import { AuthContext } from '../context/AuthContext';
import Home from '../screens/Home/Home';
import Maps from '../screens/Maps';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import ClassDetail from '../screens/Home/ClassDetail';
import Chat from '../screens/Chat';

const HomeStack = createStackNavigator()
const HomeNavigation = ({ navigation }) => {

  const state = useContext(AuthContext)
  const { currentToken } = state

  return (
    <HomeStack.Navigator initialRouteName="Login">
      {currentToken
        ? (
          <>
            <HomeStack.Screen name="HomeTabs" component={HomeTabsNavigation} options={{ headerShown: false }} />
            <HomeStack.Screen name="ClassDetail" component={ClassDetail} options={
              ({ route }) => ({ title: route.params.title })}
            />
          </>
        )
        : (
          <>
            <HomeStack.Screen name="Login" component={Login} options={{ headerShown: false }} />
            <HomeStack.Screen name="Register" component={Register} options={{ headerShown: false }} />
          </>
        )
      }
    </HomeStack.Navigator>
  )
}

const HomeTabs = createBottomTabNavigator()
const HomeTabsNavigation = () => {
  return (
    <HomeTabs.Navigator initialRouteName='Home'>
      <HomeTabs.Screen name="Home" component={Home} options={{
        headerShown: false,
        tabBarIcon: ({ color, size }) => (<Icon name='home' color={color} size={size} />)
      }} />
      <HomeTabs.Screen name="Maps" component={Maps} options={{
        headerShown: false,
        tabBarIcon: ({ color, size }) => (<Icon name='map' color={color} size={size} />)
      }} />
      <HomeTabs.Screen name="Chat" component={Chat} options={{
        headerShown: false,
        tabBarIcon: ({ color, size }) => (<Icon name='chat' color={color} size={size} />)
      }} />
      <HomeTabs.Screen name="Profile" component={Profile} options={{
        headerShown: false,
        tabBarIcon: ({ color, size }) => (<Icon name='account-circle' color={color} size={size} />)
      }} />
    </HomeTabs.Navigator>
  )
}

export default HomeNavigation