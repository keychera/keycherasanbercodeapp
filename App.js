/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import firebase from '@react-native-firebase/app';

import MainApp from './src/App'

const firebaseConfig = {
  apiKey: "AIzaSyBrkF4fMbgq4IV9lMy8xN_TqIUYBLmP-HQ",
  authDomain: "sanbercode-a7e0f.firebaseapp.com",
  databaseURL: "https://sanbercode-a7e0f.firebaseio.com",
  projectId: "sanbercode-a7e0f",
  storageBucket: "sanbercode-a7e0f.appspot.com",
  messagingSenderId: "752348763163",
  appId: "1:752348763163:web:a33470127aa8f8f67014be",
  measurementId: "G-6BJ1315QTN"
}

if(!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar hidden={true} barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        <MainApp />
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default App;
